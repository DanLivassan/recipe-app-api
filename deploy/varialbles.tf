variable "prefix" {
  default = "raa"
}

variable "ami" {
  default = "raa"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "oliveira.xc@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Username for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-api-bastion-key"
}
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "330785144428.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "330785144428.dkr.ecr.us-east-1.amazonaws.com/recipe-api-app-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
